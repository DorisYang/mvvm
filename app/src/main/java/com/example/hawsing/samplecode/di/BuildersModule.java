package com.example.hawsing.samplecode.di;

import com.example.hawsing.samplecode.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {
    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();
}
