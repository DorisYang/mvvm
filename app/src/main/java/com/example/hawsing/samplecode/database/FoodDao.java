package com.example.hawsing.samplecode.database;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

@Dao
public abstract class FoodDao {

    @Query("select * from food")
    public abstract LiveData<List<Food>> getFoodAll();

    @Query("select * from food where category_id=:category_id")
    public abstract List<Food> getFoodbyCatogory(int category_id);

    @Query("select id from food_category where name=:name")
    public abstract int getCatogoryId(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertFood(List<Food> food);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertFood(Food food);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract void insertFoodCategory(FoodCategory category);

    @Transaction
    public void insertFoodsTranscation(FoodCategory category, List<Food> food){
        insertFoodCategory(category);
        insertFood(food);
    }
}
