package com.example.hawsing.samplecode.ui.food;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.example.hawsing.samplecode.database.Food;

import java.util.List;

public class FoodDiffCallback extends DiffUtil.Callback {
    private List<Food> mOldFoodList, mNewFoodList;

    public FoodDiffCallback(List<Food> oldFoodList, List<Food> newFoodList) {
        mOldFoodList = oldFoodList;
        mNewFoodList = newFoodList;
    }

    @Override
    public int getOldListSize() {
        return mOldFoodList == null ? 0 : mOldFoodList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewFoodList == null ? 0 : mNewFoodList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldFoodList.get(oldItemPosition).id == mNewFoodList.get(newItemPosition).id;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Food oldFood = mOldFoodList.get(oldItemPosition);
        Food newFood = mNewFoodList.get(newItemPosition);
        if (oldFood.categoryId != newFood.categoryId) {
            return false;
        } else if (!oldFood.foodName.equals(newFood.foodName)) {
            return false;
        }
        return true;
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
