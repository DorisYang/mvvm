package com.example.hawsing.samplecode.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.ParametersAreNonnullByDefault;

@Entity(tableName = "food", foreignKeys = @ForeignKey(entity = FoodCategory.class,
        parentColumns = "id",
        childColumns = "category_id"), indices = {@Index("category_id"), @Index(value = "food_name", unique = true)})
public class Food {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "food_name")
    public String foodName;

    @ColumnInfo(name = "category_id")
    public int categoryId;
}
