package com.example.hawsing.samplecode.di;

import android.app.Application;

import com.example.hawsing.samplecode.AppExecutors;
import com.example.hawsing.samplecode.BasicApp;
import com.example.hawsing.samplecode.database.AppDatabase;
import com.example.hawsing.samplecode.database.FoodDao;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, BuildersModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(BasicApp application);
        AppComponent build();
    }

    void inject(BasicApp mainApp);
}
