package com.example.hawsing.samplecode.di;

import android.app.Application;

import com.example.hawsing.samplecode.AppExecutors;
import com.example.hawsing.samplecode.BasicApp;
import com.example.hawsing.samplecode.database.AppDatabase;
import com.example.hawsing.samplecode.database.FoodDao;
import com.example.hawsing.samplecode.repository.FoodRepository;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {
    @Singleton
    @Provides
    AppDatabase providesRoomDatabase(BasicApp mApplication) {
        return AppDatabase.getInstance(mApplication);
    }

    @Singleton
    @Provides
    FoodDao providesFoodDao(AppDatabase appDatabase) {
        return appDatabase.foodDao();
    }

    @Singleton
    @Provides
    FoodRepository providesFoodRepository(FoodDao foodDao, AppExecutors executor) {
        return new FoodRepository(foodDao, executor);
    }
}

