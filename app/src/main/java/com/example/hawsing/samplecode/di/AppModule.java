package com.example.hawsing.samplecode.di;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.example.hawsing.samplecode.AppExecutors;
import com.example.hawsing.samplecode.BasicApp;
import com.example.hawsing.samplecode.ui.food.FoodViewModel;
import com.example.hawsing.samplecode.repository.FoodRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = {RoomModule.class})
class AppModule {

}
