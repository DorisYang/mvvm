package com.example.hawsing.samplecode.ui.food;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.example.hawsing.samplecode.R;
import com.example.hawsing.samplecode.database.Food;

import java.util.List;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.MyViewHolder> {
    public List<Food> mFoodList;

    public FoodListAdapter(List<Food> foodList) {
        mFoodList = foodList;
    }

    public void setFoodList(List<Food> foodList) {
        FoodDiffCallback foodDiffCallback = new FoodDiffCallback(mFoodList, foodList);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(foodDiffCallback);
        mFoodList = foodList;
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_food, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setBinding(mFoodList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFoodList == null ? 0 : mFoodList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding mBinding;

        public MyViewHolder(View view) {
            super(view);
            mBinding = DataBindingUtil.bind(view);
        }

        public void setBinding(Food food) {
            mBinding.setVariable(BR.food, food);
            mBinding.executePendingBindings();
        }

    }
}
