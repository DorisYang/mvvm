package com.example.hawsing.samplecode.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.hawsing.samplecode.AppExecutors;
import com.example.hawsing.samplecode.database.AppDatabase;
import com.example.hawsing.samplecode.database.Food;
import com.example.hawsing.samplecode.database.FoodCategory;
import com.example.hawsing.samplecode.database.FoodDao;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FoodRepository {
    private final FoodDao foodDao;
    private final AppExecutors executor;

    @Inject
    public FoodRepository(FoodDao foodDao, AppExecutors executor) { // FoodApi
        this.foodDao = foodDao;
        this.executor = executor;
    }

    public LiveData<List<Food>> getListFood() {
        return foodDao.getFoodAll();
    }

    public void insertFood(List<Food> foods, String categoryName) {
        executor.diskIO().execute(() -> {
            // running in a background thread
            if (getCategoryId(categoryName) > 0) {
                foods.forEach(food -> food.categoryId = getCategoryId(categoryName));
                foodDao.insertFood(foods);
            }
        });
    }

    public void insertFood(Food food, String categoryName) {
        executor.diskIO().execute(() -> {
            if (getCategoryId(categoryName) > 0) {
                food.categoryId = getCategoryId(categoryName);
                foodDao.insertFood(food);
            }
        });
    }

    public int getCategoryId(String name) {
        return foodDao.getCatogoryId(name);
    }

    public void insertFoodCategory(FoodCategory category) {
        executor.diskIO().execute(() -> {
            // running in a background thread
            foodDao.insertFoodCategory(category);
        });
    }

}
