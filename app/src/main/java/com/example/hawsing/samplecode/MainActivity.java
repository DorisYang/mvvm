package com.example.hawsing.samplecode;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.hawsing.samplecode.database.AppDatabase;
import com.example.hawsing.samplecode.database.Food;
import com.example.hawsing.samplecode.databinding.ActivityFoodBinding;
import com.example.hawsing.samplecode.ui.food.FoodListAdapter;
import com.example.hawsing.samplecode.ui.food.FoodViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {
    String TAG = "Doris";

    @Inject
    FoodViewModel foodViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityFoodBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_food);
        mBinding.setViewModel(foodViewModel);
        mBinding.setLifecycleOwner(this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        foodViewModel.init();

        foodViewModel.insertFoodCategory("水果");
        foodViewModel.insertFoodCategory("飲料");
        foodViewModel.insertFood();

        FoodListAdapter adapter = new FoodListAdapter(foodViewModel.getFoodlist().getValue());

        mBinding.setAdapter(adapter);


        foodViewModel.getFoodlist().observe(this, events -> {
            adapter.setFoodList(events);
            events.forEach(food -> {
                Log.d(TAG, food.foodName + ":" + food.categoryId);
            });

        });
    }

}
