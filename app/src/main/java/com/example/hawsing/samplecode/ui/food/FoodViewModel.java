package com.example.hawsing.samplecode.ui.food;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.hawsing.samplecode.database.Food;
import com.example.hawsing.samplecode.database.FoodCategory;
import com.example.hawsing.samplecode.repository.FoodRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;

public class FoodViewModel extends ViewModel {
    public LiveData<List<Food>> foodlist;
    public MutableLiveData<String> food = new MutableLiveData<>();
    private FoodRepository foodRepo;
    private String[] foods1 = {"葡萄", "芒果", "水蜜桃"};
    private String[] foods2 = {"黑麥汁", "芒果汁", "檸檬汁", "麥茶"};
    public MutableLiveData<String> hello = new MutableLiveData<>() ;

    @Inject
    public FoodViewModel(FoodRepository foodRepository) {
        foodRepo = foodRepository;
    }

    public void init() {
        if (foodlist != null) {
            return;
        }
        foodlist = foodRepo.getListFood();
    }

    public LiveData<List<Food>> getFoodlist() {
        return foodlist;
    }

    public void getHello(){
        if(foodlist != null && foodlist.getValue()!= null && foodlist.getValue().size() > 0)
            hello.setValue(foodlist.getValue().get(0).foodName);
    }

    public void insertFood() {
        addFood(foods1, "水果");
        addFood(foods2, "飲料");

    }


    public void addFood(String[] data, String categoryName) {
        List<Food> foods = new ArrayList<>();
        Arrays.stream(data).forEach(s -> {
            Food food = new Food();
            food.foodName = s;
            foods.add(food);
        });
        foodRepo.insertFood(foods, categoryName);
    }

    public void addFood() {
        Food mFood = new Food();
        mFood.foodName = food.getValue();
        foodRepo.insertFood(mFood, "水果");
    }

    public void addFood(CharSequence s, int start, int before, int count){
        food.setValue(s.toString());
    }

    public void insertFoodCategory(String name) {
        FoodCategory category = new FoodCategory(name);
        foodRepo.insertFoodCategory(category);
    }
}
