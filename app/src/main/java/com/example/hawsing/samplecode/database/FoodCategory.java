package com.example.hawsing.samplecode.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "food_category", indices = {@Index(value = "name", unique = true)})
public class FoodCategory {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public String name;

    public FoodCategory(String name){
        this.name = name;
        id = 0;
    }

}
